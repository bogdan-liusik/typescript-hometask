"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFighterDetails = exports.getFighters = void 0;
const apiHelper_1 = require("../helpers/apiHelper");
async function getFighters() {
    try {
        const endpoint = 'fighters.json';
        const apiResult = await apiHelper_1.callApi(endpoint, 'GET');
        return apiResult;
    }
    catch (error) {
        throw error;
    }
}
exports.getFighters = getFighters;
async function getFighterDetails(id) {
    // endpoint - `details/fighter/${id}.json`;
    try {
        const endpoint = `details/fighter/${id}.json`;
        const apiResult = await apiHelper_1.callApi(endpoint, 'GET');
        return apiResult;
    }
    catch (error) {
        throw error;
    }
}
exports.getFighterDetails = getFighterDetails;
