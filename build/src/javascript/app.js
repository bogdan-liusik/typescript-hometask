"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.startApp = void 0;
const fightersService_1 = require("./services/fightersService");
const fightersView_1 = require("./fightersView");
const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');
async function startApp() {
    try {
        loadingElement.style.visibility = 'visible';
        const fighters = (await fightersService_1.getFighters());
        const fightersElement = fightersView_1.createFighters(fighters);
        rootElement.appendChild(fightersElement);
    }
    catch (error) {
        console.warn(error);
        rootElement.innerText = 'Failed to load data';
    }
    finally {
        loadingElement.style.visibility = 'hidden';
    }
}
exports.startApp = startApp;
