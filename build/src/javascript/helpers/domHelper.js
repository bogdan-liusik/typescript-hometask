"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createElement = void 0;
function createElement({ tagName, className, attributes }) {
    const element = document.createElement(tagName);
    if (className) {
        element.classList.add(className);
    }
    if (attributes) {
        Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
    }
    return element;
}
exports.createElement = createElement;
