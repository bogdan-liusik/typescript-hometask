"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.callApi = void 0;
const mockData_1 = require("./mockData");
const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI = true;
async function callApi(endpoint, method) {
    const url = API_URL + endpoint;
    const options = {
        method,
    };
    return useMockAPI
        ? fakeCallApi(endpoint)
        : fetch(url, options)
            .then((response) => response.ok
            ? response.json().then(result => JSON.parse(atob(result.content)))
            : Promise.reject(Error('Failed to load')))
            .catch((error) => {
            throw error;
        });
}
exports.callApi = callApi;
async function fakeCallApi(endpoint) {
    const response = endpoint === 'fighters.json' ? mockData_1.fighters : getFighterById(endpoint);
    return new Promise((resolve, reject) => {
        setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
    });
}
function getFighterById(endpoint) {
    const start = endpoint.lastIndexOf('/');
    const end = endpoint.lastIndexOf('.json');
    const id = endpoint.substring(start + 1, end);
    return mockData_1.fightersDetails.find((it) => it._id === id);
}
