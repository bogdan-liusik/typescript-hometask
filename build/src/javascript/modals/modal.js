"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.showModal = void 0;
const domHelper_1 = require("../helpers/domHelper");
function showModal({ title, bodyElement }) {
    const root = getModalContainer();
    const modal = createModal(title, bodyElement);
    root?.append(modal);
}
exports.showModal = showModal;
function getModalContainer() {
    return document.getElementById('root');
}
function createModal(title, bodyElement) {
    const layer = domHelper_1.createElement({
        tagName: 'div',
        className: 'modal-layer'
    });
    const modalContainer = domHelper_1.createElement({
        tagName: 'div',
        className: 'modal-root'
    });
    const header = createHeader(title);
    modalContainer.append(header, bodyElement);
    layer.append(modalContainer);
    return layer;
}
function createHeader(title) {
    const headerElement = domHelper_1.createElement({
        tagName: 'div',
        className: 'modal-header'
    });
    const titleElement = domHelper_1.createElement({ tagName: 'span' });
    const closeButton = domHelper_1.createElement({
        tagName: 'div',
        className: 'close-btn'
    });
    titleElement.innerText = title;
    closeButton.innerText = '×';
    closeButton.addEventListener('click', hideModal);
    headerElement.append(title, closeButton);
    return headerElement;
}
function hideModal(event) {
    const modal = document.getElementsByClassName('modal-layer')[0];
    modal?.remove();
}
