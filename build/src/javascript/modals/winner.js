"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.showWinnerModal = void 0;
const domHelper_1 = require("../helpers/domHelper");
const modal_1 = require("./modal");
function showWinnerModal(fighter) {
    const title = 'Winner';
    const bodyElement = createWinnerContainer(fighter);
    modal_1.showModal({ title, bodyElement });
}
exports.showWinnerModal = showWinnerModal;
function createWinnerContainer(winner) {
    const { name, source } = winner;
    const winnerContainer = domHelper_1.createElement({ tagName: 'div', className: 'winner-modal-body', attributes: {} });
    const imageElement = domHelper_1.createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source } });
    const nameElement = domHelper_1.createElement({ tagName: 'span', className: 'name', attributes: {} });
    nameElement.innerText = name;
    winnerContainer.append(imageElement);
    winnerContainer.append(nameElement);
    return winnerContainer;
}
