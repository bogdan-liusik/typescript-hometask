"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.showFighterDetailsModal = void 0;
const domHelper_1 = require("../helpers/domHelper");
const modal_1 = require("./modal");
function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    modal_1.showModal({ title, bodyElement });
}
exports.showFighterDetailsModal = showFighterDetailsModal;
function createFighterDetails(fighter) {
    const { name, attack, defense, health, source } = fighter;
    const fighterDetails = domHelper_1.createElement({
        tagName: 'div',
        className: 'modal-body'
    });
    const createSpanElement = (className) => {
        return domHelper_1.createElement({ tagName: 'span', className });
    };
    const nameElement = createSpanElement('fighter-name');
    const healthElement = createSpanElement('fighter-property');
    const attackElement = createSpanElement('fighter-property');
    const defenseElement = createSpanElement('fighter-property');
    const imageElement = domHelper_1.createElement({
        tagName: 'img',
        className: 'fighter-image',
        attributes: { src: source }
    });
    nameElement.innerText = name;
    healthElement.innerText = `Health: ${health}`;
    attackElement.innerText = `Attack: ${attack}`;
    defenseElement.innerText = `Defence: ${defense}`;
    fighterDetails.append(nameElement, healthElement, attackElement, defenseElement, imageElement);
    return fighterDetails;
}
