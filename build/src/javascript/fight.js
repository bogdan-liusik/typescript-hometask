"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBlockPower = exports.getHitPower = exports.getDamage = exports.fight = void 0;
const random_number_1 = require("./helpers/random-number");
function fight(firstFighter, secondFighter) {
    let { health: firstFighterHealth } = firstFighter;
    let { health: secondFighterHealth } = secondFighter;
    while (secondFighterHealth > 0) {
        firstFighterHealth -= getDamage(secondFighter, firstFighter);
        if (firstFighterHealth <= 0) {
            return secondFighter;
        }
        secondFighterHealth -= getDamage(firstFighter, secondFighter);
    }
    return firstFighter;
}
exports.fight = fight;
function getDamage(attacker, defender) {
    const hitPower = getHitPower(attacker);
    const blockPower = getBlockPower(defender);
    return blockPower > hitPower ? 0 : hitPower - blockPower;
}
exports.getDamage = getDamage;
function getHitPower(fighter) {
    const criticalHitChance = random_number_1.getRandomNumber(1, 2);
    return fighter.attack * criticalHitChance;
}
exports.getHitPower = getHitPower;
function getBlockPower(fighter) {
    const dodgeChance = random_number_1.getRandomNumber(1, 2);
    return fighter.defense * dodgeChance;
}
exports.getBlockPower = getBlockPower;
