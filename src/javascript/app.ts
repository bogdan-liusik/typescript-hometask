import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';
import { Fighter } from "./interfaces/fighter";

const rootElement: HTMLDivElement = <HTMLDivElement>document.getElementById('root');
const loadingElement: HTMLDivElement = <HTMLDivElement>document.getElementById('loading-overlay');

export async function startApp(): Promise<void> {
  try {
    loadingElement.style.visibility = 'visible';
    
    const fighters: Fighter[] = <Fighter[]>(await getFighters());
    const fightersElement: HTMLDivElement = createFighters(fighters);

    rootElement.appendChild(fightersElement);
  } catch (error) {
    console.warn(error);
    rootElement.innerText = 'Failed to load data';
  } finally {
    loadingElement.style.visibility = 'hidden';
  }
}
