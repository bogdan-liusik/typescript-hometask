import { HtmlElement } from "../interfaces/html-element";

export function createElement<T extends HTMLElement>({ tagName, className, attributes } : HtmlElement) : T {
  const element: T = <T>document.createElement(tagName);
  
  if (className) {
    element.classList.add(className);
  }

  if(attributes){
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  }

  return element;
}
