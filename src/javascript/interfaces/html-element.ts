export interface HtmlElement {
    tagName: string;
    className?: string;
    attributes?: { [key: string]: string; };
}
