export interface ModalDetails {
    title: string;
    bodyElement: HTMLElement;
}
