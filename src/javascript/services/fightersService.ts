import { callApi } from '../helpers/apiHelper';
import { FighterDetails } from '../interfaces/fighter-details';
import { Fighter } from '../interfaces/fighter';

export async function getFighters(): Promise<Fighter[] | FighterDetails> {
  try {
    const endpoint: string = 'fighters.json';
    const apiResult: Fighter[] | FighterDetails = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string): Promise<FighterDetails> {
  // endpoint - `details/fighter/${id}.json`;
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult: Fighter[] | FighterDetails = await callApi(endpoint, 'GET');

    return <FighterDetails>apiResult;
  } catch(error) {
    throw error;
  }
}

