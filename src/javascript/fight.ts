import { FighterDetails } from "./interfaces/fighter-details";
import { Fighter } from "./interfaces/fighter";
import { getRandomNumber } from "./helpers/random-number";

export function fight(firstFighter: FighterDetails, secondFighter: FighterDetails): Fighter {
    let { health: firstFighterHealth } = firstFighter;
    let { health: secondFighterHealth } = secondFighter;

    while(secondFighterHealth > 0) {
        firstFighterHealth -= getDamage(secondFighter, firstFighter);

        if(firstFighterHealth <= 0) {
            return secondFighter;
        }
        secondFighterHealth -= getDamage(firstFighter, secondFighter);
    }

    return firstFighter;
}

export function getDamage(attacker: FighterDetails, defender: FighterDetails): number {
    const hitPower: number = getHitPower(attacker);
    const blockPower: number = getBlockPower(defender);

    return blockPower > hitPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter: FighterDetails): number {
    const criticalHitChance = getRandomNumber(1, 2);
    return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: FighterDetails): number {
    const dodgeChance = getRandomNumber(1, 2);
    return fighter.defense * dodgeChance;
}
