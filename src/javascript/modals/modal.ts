import { createElement } from '../helpers/domHelper';
import {ModalDetails} from '../interfaces/modal-details';

export function showModal({ title, bodyElement }: ModalDetails): void {
  const root: HTMLDivElement = getModalContainer();
  const modal: HTMLElement = createModal(title, bodyElement);
  
  root?.append(modal);
}

function getModalContainer(): HTMLDivElement {
  return <HTMLDivElement>document.getElementById('root');
}

function createModal(title: string, bodyElement: HTMLElement): HTMLDivElement {
  const layer: HTMLDivElement = createElement<HTMLDivElement>({
    tagName: 'div',
    className: 'modal-layer'
  });

  const modalContainer: HTMLDivElement = createElement<HTMLDivElement>({
    tagName: 'div',
    className: 'modal-root'
  });

  const header: HTMLElement = createHeader(title);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string): HTMLDivElement {
  const headerElement: HTMLDivElement = createElement<HTMLDivElement>({
    tagName: 'div',
    className: 'modal-header'
  });

  const titleElement: HTMLSpanElement = createElement<HTMLSpanElement>({ tagName: 'span' });

  const closeButton: HTMLDivElement = createElement<HTMLDivElement>({
    tagName: 'div',
    className: 'close-btn'
  });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  closeButton.addEventListener('click', hideModal);
  headerElement.append(title, closeButton);
  
  return headerElement;
}

function hideModal(event: Event): void {
  const modal: HTMLDivElement = <HTMLDivElement>document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
