import { Fighter } from "../interfaces/fighter";
import {createElement} from "../helpers/domHelper";
import {showModal} from "./modal";

export function showWinnerModal(fighter: Fighter): void {
    const title = 'Winner';
    const bodyElement = createWinnerContainer(fighter);
    showModal({ title, bodyElement });
}

function createWinnerContainer(winner: Fighter): HTMLDivElement {
    const { name, source } = winner;

    const winnerContainer: HTMLDivElement = <HTMLDivElement>createElement({ tagName: 'div', className: 'winner-modal-body', attributes : {}});
    const imageElement: HTMLImageElement = <HTMLImageElement>createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source }});
    const nameElement: HTMLSpanElement = <HTMLSpanElement>createElement({ tagName: 'span', className: 'name', attributes : {}});

    nameElement.innerText = name;

    winnerContainer.append(imageElement);
    winnerContainer.append(nameElement);

    return winnerContainer;
}
