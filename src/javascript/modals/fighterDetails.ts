import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { FighterDetails } from '../interfaces/fighter-details';

export  function showFighterDetailsModal(fighter: FighterDetails) {
  const title: string = 'Fighter info';
  const bodyElement: HTMLDivElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: FighterDetails): HTMLDivElement {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails: HTMLDivElement = createElement<HTMLDivElement>({
    tagName: 'div',
    className: 'modal-body'
  });

  const createSpanElement = (className: string): HTMLSpanElement => {
    return createElement<HTMLSpanElement>({ tagName: 'span', className });
  }

  const nameElement: HTMLSpanElement = createSpanElement('fighter-name');
  const healthElement: HTMLSpanElement = createSpanElement('fighter-property');
  const attackElement: HTMLSpanElement = createSpanElement('fighter-property');
  const defenseElement: HTMLSpanElement = createSpanElement('fighter-property');
  const imageElement: HTMLImageElement = createElement<HTMLImageElement>({
    tagName: 'img',
    className: 'fighter-image',
    attributes: {src: source}
  });

  nameElement.innerText = name;
  healthElement.innerText = `Health: ${health}`;
  attackElement.innerText = `Attack: ${attack}`;
  defenseElement.innerText = `Defence: ${defense}`;

  fighterDetails.append(nameElement, healthElement, attackElement, defenseElement, imageElement);

  return fighterDetails;
}
